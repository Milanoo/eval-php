<?php

namespace App\API\Entity;

class Country implements \JsonSerializable
{
    private $id;
    private $name;
    private $cityId;

    public function jsonSerialize():array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'cityId' => $this->getCityId(),
        ];
    }

    public function getId():?int { return $this->id; }
    public function setId(?int $id):void { $this->id = $id;}

    public function getName():string { return $this->name; }
    public function setName(string $name):void { $this->name = $name;}

    public function getCityId():string { return $this->cityId; }
    public function setCityId(string $cityId):void { $this->cityId = $cityId;}
}