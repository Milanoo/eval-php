<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="country">
  <div class="country__id country__common">id: <?= $id ;?></div>
  <div class="country__name country__common">name: <?= $name ;?></div>
  <div class="country__city country__common">city id: <?= $city_id ;?></div>
</div>
</body>
</html>
<style lang="css">
  .country {
    background: red;
    text-align: center;
    border: 1px solid black;
  }
  .country__id {
    background: aqua;
  }
  .country__name {
    background: orange;
  }
  .country__city {
    background: aqua;
  }
  .country__common {
    height: 100px;
    font-size: 20px;
    font-family: "Abyssinica SIL";
    font-style: oblique;
    padding-top: 20px;
  }
</style>